# pip install xmltodict
import xmltodict
from matplotlib import pyplot as plt
import numpy
import subprocess
import os
import random

# Useful const
BYTES_TO_BIT = 8
TO_MEGA = 1000000

# Cmd arguments used by ns3 simulation
SIMULATION_TIME_LENGHT = 8.0
SEND_SIZE0 = 1408
SEND_SIZE2 = 128
seed_value = random.randint(1, 1000)
file_name = ["scratch/FirstAssignment1.xml", "scratch/FirstAssignment2.xml"]
error_rate = [0.0001, 0.01]

# Check if FirstAssignment.cc e FirstAssignment.py are in scratch directory
py_file_path = os.path.abspath(__file__)
scratch_dir = os.path.dirname(py_file_path)
py_check = scratch_dir.find("scratch")
cc_check = os.path.exists(os.path.join(scratch_dir, "FirstAssignment.cc"))
assert py_check, "FirstAssignment.py is not inside scratch dir!"
assert cc_check, "FirstAssignment.cc is not inside scratch dir!"

def getData(path_to_xml_file:str):
    # Parse the xml files
    with open(path_to_xml_file) as xml_file:
        monitor_dict = xmltodict.parse(xml_file.read())
    FlowMonitor = monitor_dict["FlowMonitor"]

    # Extract from Ipv4FlowClassifier flowId, sourceAddress, destinationAddress
    flow_directions_dic = {}
    Ipv4FlowClassifier = FlowMonitor["Ipv4FlowClassifier"]
    for flow_idx in Ipv4FlowClassifier["Flow"]:
        ID = int(flow_idx["@flowId"])
        sourceAddress = flow_idx["@sourceAddress"]
        destinationAddress = flow_idx["@destinationAddress"]
        direction = f"{sourceAddress}\n"+\
                    " |\n" +\
                    " v\n" +\
                    f"{destinationAddress}"
        
        flow_directions_dic.update({f"{ID}": direction})
        
    # Extract from FlowStats flowId, txBytes, rxBytes, lostPackets, txPackets, rxPackets
    data_dic = {}
    FlowStats = FlowMonitor["FlowStats"]
    for flow_idx in FlowStats["Flow"]:
        ID = flow_idx["@flowId"]
        txBytes = int(flow_idx["@txBytes"])
        rxBytes = int(flow_idx["@rxBytes"])
        lostPackets = int(flow_idx["@lostPackets"])
        txPackets = int(flow_idx["@txPackets"])
        rxPackets = int(flow_idx["@rxPackets"])
        rxThroughput = (rxBytes * BYTES_TO_BIT)/(SIMULATION_TIME_LENGHT * TO_MEGA)
        txThroughput = (txBytes * BYTES_TO_BIT)/(SIMULATION_TIME_LENGHT * TO_MEGA)
        data_dic.update({f"{ID}": {"txBytes": txBytes, "rxBytes": rxBytes, "lostPackets": lostPackets,
                                "txPackets": txPackets, "rxPackets": rxPackets, "rxThroughput": rxThroughput,
                                "txThroughput": txThroughput, "direction": flow_directions_dic[ID]}})
    return data_dic

# Organize data in lists for later use in matplot
def organizeData(data_dic:dict):
    txBytes_list = [None] * len(data_dic)
    rxBytes_list = [None] * len(data_dic)
    lostPackets_list = [None] * len(data_dic)
    txPackets_list = [None] * len(data_dic)
    rxPackets_list = [None] * len(data_dic)
    rxThroughput_list = [None] * len(data_dic)
    txThroughput_list = [None] * len(data_dic)
    directions_list  = [None] * len(data_dic)
    for i in data_dic:
        txBytes_list[int(i)-1] = data_dic[i]["txBytes"]
        rxBytes_list[int(i)-1] = data_dic[i]["txBytes"]
        lostPackets_list[int(i)-1] = data_dic[i]["lostPackets"]
        txPackets_list[int(i)-1] = data_dic[i]["txPackets"]
        rxPackets_list[int(i)-1] = data_dic[i]["rxPackets"]
        txThroughput_list[int(i)-1] = data_dic[i]["txThroughput"]
        rxThroughput_list[int(i)-1] = data_dic[i]["rxThroughput"]
        directions_list[int(i)-1] = data_dic[i]['direction']

    total_throughput = sum(rxThroughput_list)

    return directions_list, rxThroughput_list, total_throughput
    


def main():

    # Launch n times the simulation in FirstAssignment.cc
    for i in range(2):
        command = f"scratch/FirstAssignment.cc --simulationLenght={SIMULATION_TIME_LENGHT} --sendSize0={SEND_SIZE0}" +\
            f" --sendSize2={SEND_SIZE2} --seedValue={seed_value} --fileName={file_name[i]} --errorRate={error_rate[i]}"
        simulation = subprocess.Popen(["./ns3", "run", command])
        simulation.communicate()

    # Use functions to retrieve data from xml
    data1 = getData("scratch/FirstAssignment1.xml")
    directions_list1, rxThroughput_list1, total_throughput1 = organizeData(data1)

    data2 = getData("scratch/FirstAssignment2.xml")
    directions_list2, rxThroughput_list2, total_throughput2 = organizeData(data2)

    # Use matplotlib to draw graphs
    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(8, 6))

    colors = [numpy.random.rand(3,) for i in range(len(data1))]

    ax1.bar(directions_list1, rxThroughput_list1, color=colors)
    ax1.set_ylabel('Throughput [Mbit/s]')
    ax1.tick_params(axis='x', which='major', labelsize=8)
    ax1.grid(axis='y', linestyle='dashed', linewidth=0.5)
    x_coord = ax1.get_xlim()[1]
    y_coord = ax1.get_ylim()[1]
    description = f"""
    Channel datarate: 100 Mbit/s
    Total throughput {total_throughput1:.2f} Mbit/s'
    Error model: Uniform
    Error unit: per packet
    Error rate: {error_rate[0]}"""
    ax1.text(x_coord, y_coord, description, fontsize=10, va='top')

    ax2.bar(directions_list2, rxThroughput_list2, color=colors)
    ax2.set_ylabel('Throughput [Mbit/s]')
    ax2.tick_params(axis='x', which='major', labelsize=8)
    ax2.grid(axis='y', linestyle='dashed', linewidth=0.5)
    x_coord = ax2.get_xlim()[1]
    y_coord = ax2.get_ylim()[1]
    description = f"""
    Channel datarate: 100 Mbit/s
    Total throughput {total_throughput2:.2f} Mbit/s'
    Error model: Uniform
    Error unit: per packet
    Error rate: {error_rate[1]}"""
    ax2.text(x_coord, y_coord, description, fontsize=10, va='top')

    plt.tight_layout()
    plt.savefig("scratch/FirstAssignment.png")

if __name__ == "__main__":
    main()
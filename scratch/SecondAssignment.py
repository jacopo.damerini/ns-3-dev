from matplotlib import pyplot as plt
import subprocess
import os
import re
import time
import typing

# Useful const
SIMULATION_TIME_LENGHT = 10.0
start_time = time.time()

# Experiment0 -> 10 tests
n_test0 = 5
distanceS0A0_list = [30, 70, 80, 85, 95]
distanceA0A1_list = [10000] * n_test0
distanceA1S1_list = [1] * n_test0
wifi_type_list = (["Spectrum"]) * n_test0
channelNumber0_list = [1] * n_test0
channelNumber1_list = [6] * n_test0
enableNet1AppLayer_list = [False] * n_test0

n_test1 = 5
distanceS0A0_list.extend([30, 70, 80, 85, 95])
distanceA0A1_list.extend([10000] * n_test1)
distanceA1S1_list.extend([1] * n_test1)
wifi_type_list.extend(["Yans"] * n_test1)
channelNumber0_list.extend([1] * n_test1)
channelNumber1_list.extend([6] * n_test1)
enableNet1AppLayer_list.extend([False] * n_test1)




# Experiment1 -> 10 tests
n_test2 = 5
distanceS0A0_list.extend([1] * n_test2) 
distanceA0A1_list.extend([10, 110, 120, 125, 130] * n_test2)
distanceA1S1_list.extend([1] * n_test2)
wifi_type_list.extend(["Spectrum"] * n_test2)
channelNumber0_list.extend([1] * n_test2)
channelNumber1_list.extend([6] * n_test2)
enableNet1AppLayer_list.extend([True] * n_test2)

n_test3 = 5
distanceS0A0_list.extend([1] * n_test3) 
distanceA0A1_list.extend([10, 110, 120, 125, 130] * n_test3)
distanceA1S1_list.extend([1] * n_test3)
wifi_type_list.extend(["Yans"] * n_test3)
channelNumber0_list.extend([1] * n_test3)
channelNumber1_list.extend([6] * n_test3)
enableNet1AppLayer_list.extend([True] * n_test3)




# Experiment3 -> 10 tests
n_test4 = 5
distanceS0A0_list.extend([1] * n_test4) 
distanceA0A1_list.extend([10, 110, 120, 125, 130] * n_test4)
distanceA1S1_list.extend([1] * n_test4)
wifi_type_list.extend(["Spectrum"] * n_test4)
channelNumber0_list.extend([1] * n_test4) 
channelNumber1_list.extend([2] * n_test4)
enableNet1AppLayer_list.extend([True] * n_test4)

n_test5 = 5
distanceS0A0_list.extend([1] * n_test5) 
distanceA0A1_list.extend([10, 110, 120, 125, 130] * n_test5)
distanceA1S1_list.extend([1] * n_test5)
wifi_type_list.extend(["Yans"] * n_test5)
channelNumber0_list.extend([1] * n_test5)
channelNumber1_list.extend([2] * n_test5)
enableNet1AppLayer_list.extend([True] * n_test5)



num_simulations = n_test0 + n_test1 + n_test2 + n_test3 + n_test4 + n_test5
simID_list = list(range(0, num_simulations+1))
simulation_time_list = [SIMULATION_TIME_LENGHT] * num_simulations


simulations = zip(simID_list, simulation_time_list, distanceS0A0_list, distanceA0A1_list,
                  distanceA1S1_list, wifi_type_list, channelNumber0_list, channelNumber1_list, enableNet1AppLayer_list)

experiment0 = []
experiment1 = []
experiment2 = []

time0 = []
time1 = []
time2 = []

normalized_times = []



# Check if SecondAssignment.cc e SecondAssignment.py are in scratch directory
py_file_path = os.path.abspath(__file__)
scratch_dir = os.path.dirname(py_file_path)
py_check = scratch_dir.find("scratch")
cc_check = os.path.exists(os.path.join(scratch_dir, "SecondAssignment.cc"))
assert py_check, "SecondAssignment.py is not inside scratch dir!"
assert cc_check, "SecondAssignment.cc is not inside scratch dir!"


def getThroughput(simID: int, n_network: int):
    # Parse the txt files
    with open(f"scratch/output_sim{simID}.txt") as file:
        sim_output = file.read() 
        pattern = f"Throughput Network{n_network}: (\d+\.\d+)"
        match = re.search(pattern, sim_output)
        value = -1.0
        if match:
            value = float(match.group(1))
        return value

def getTime(simID: int):
    with open(f"scratch/output_sim{simID}.txt") as file:
        sim_output = file.read() 
        pattern = "Sim seconds: (\d+\.\d+)"
        match = re.search(pattern, sim_output)
        value = -1.0
        if match:
            value = float(match.group(1))
        return round(value, 2)

def getNormTime(throughput: float , time: float):
    if throughput <= 15:
        return 0
    else:
        return (time / throughput)

def main():
    # Launch n times the simulation in SecondAssignment.cc
    for sim in simulations:
        command = f"scratch/SecondAssignment.cc --simID={sim[0]} --simulationTime={sim[1]}" +\
            f" --distanceS0A0={sim[2]} --distanceA0A1={sim[3]} --distanceA1S1={sim[4]} --wifiType={sim[5]}" +\
            f" --chNumber0={sim[6]} --chNumber1={sim[7]} --enableNet1AppLayer={sim[8]}"
        simulation = subprocess.Popen(["./ns3", "run", command])
        simulation.communicate()

    # Process the result of the simulations
    for simID in range(num_simulations):
        if 0 <= simID < 10:
            trough0 = getThroughput(simID, 0)
            time = getTime(simID)
            norm_time = getNormTime(trough0, time)
            experiment0.append(trough0)
            time0.append(time)
            normalized_times.append(norm_time)
        elif 10 <= simID < 20:
            trough0 = getThroughput(simID, 0)
            trough1 = getThroughput(simID, 1)
            tot_thr = trough0 + trough1
            time = getTime(simID)
            norm_time = getNormTime(tot_thr, time)
            experiment1.append((trough0, trough1))
            time1.append(time)
            normalized_times.append(norm_time)
        elif 20 <= simID < 30:
            trough0 = getThroughput(simID, 0)
            trough1 = getThroughput(simID, 1)
            tot_thr = trough0 + trough1
            time = getTime(simID)
            norm_time = getNormTime(tot_thr, time)
            experiment2.append((trough0, trough1))
            time2.append(time)
            normalized_times.append(norm_time)


    # Graph the data
    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(8, 6))
    ax1.plot(distanceS0A0_list[:5], experiment0[:5], color='red', label='Spectrum0', marker='.')
    ax1.plot(distanceS0A0_list[5:10], experiment0[5:10], color='blue', label='Yans0', marker='.')
    ax1.set_ylabel('Throughput [Mbit/s]')
    ax1.set_xlabel('Distance between ST0 e AP0 [m]')
    ax1.set_title("Exp0: Variation in ST0 and AP0 distance", fontsize='small')
    ax1.legend(fontsize='x-small')
 
    ax2.plot(distanceA0A1_list[15:20], [pair[0] for pair in experiment1[5:10]], color='blue', label='Yans0', marker='.')
    ax2.plot(distanceA0A1_list[10:15], [pair[0] for pair in experiment1[:5]], color='red', label='Spectrum0', marker='.')
    ax2.plot(distanceA0A1_list[10:15], [pair[1] for pair in experiment1[:5]], color='orange', label='Spectrum1', marker='.')
    ax2.plot(distanceA0A1_list[15:20], [pair[1] for pair in experiment1[5:10]], color='skyblue', label='Yans1', marker='.')   
    ax2.set_ylabel('Throughput [Mbit/s]')
    ax2.set_xlabel('Distance between AP0 e AP1 [m]')
    ax2.set_title("Exp1: Variation of AP0 and AP1 distance (using non-interfering channels)", fontsize='small')
    ax2.legend(fontsize='x-small')

    ax3.plot(distanceA0A1_list[20:25], [pair[0] for pair in experiment2[:5]], color='red', label='Spectrum0', marker='.')
    ax3.plot(distanceA0A1_list[20:25], [pair[1] for pair in experiment2[:5]], color='orange', label='Spectrum1', marker='.')
    ax3.plot(distanceA0A1_list[25:30], [pair[0] for pair in experiment2[5:10]], color='blue', label='Yans0', marker='.')
    ax3.plot(distanceA0A1_list[25:30], [pair[1] for pair in experiment2[5:10]], color='skyblue', label='Yans1', marker='.')
    ax3.set_ylabel('Throughput [Mbit/s]')   
    ax3.set_xlabel('Distance between AP0 e AP1 [m]')
    ax3.set_title("Exp2: Variation of AP0 and AP1 distance (using interfering channels)", fontsize='small')
    ax3.legend(fontsize='x-small')

    plt.tight_layout()
    plt.savefig("scratch/SecondAssignment0.png")

    _ = list(range(0, 11))
    fig, ax4 = plt.subplots(figsize=(6, 4))
    ax4.bar(_[:5], time0[:5], color='red', label='Spectrum')
    ax4.bar(_[5:10], time0[5:10], color='blue', label='Yans')
    for i, v in enumerate(time0):
        plt.text(i, v + 0.05, experiment0[i], color='black', ha='center')
    ax4.set_ylabel('Sim time [s]')
    ax4.set_xticklabels([])
    ax4.set_xticks([])
    ax4.grid(axis='y', linestyle='dashed', linewidth=0.5)
    ax4.set_title("Exp0: CPU time and total throughput", fontsize='small')
    ax4.legend(fontsize='x-small', loc='upper left', bbox_to_anchor=(0.8, 0.0))

    fig, ax5 = plt.subplots(figsize=(6, 4))
    ax5.bar(_[:5], time1[:5], color='red', label='Spectrum')
    ax5.bar(_[5:10], time1[5:10], color='blue', label='Yans')
    for i, v in enumerate(time1):
        tot_throug = round(experiment2[i][0] + experiment2[i][1], 2)
        plt.text(i, v + 0.05, tot_throug, color='black', ha='center')
    ax5.set_ylabel('Sim time [s]')
    ax5.set_xticklabels([])
    ax5.set_xticks([])
    ax5.grid(axis='y', linestyle='dashed', linewidth=0.5)
    ax5.set_title("Exp1: CPU time and total throughput", fontsize='small')
    ax5.legend(fontsize='x-small', loc='upper left', bbox_to_anchor=(0.8, 0.0))

    fig, ax6 = plt.subplots(figsize=(6, 4))
    ax6.bar(_[:5], time2[:5], color='red', label='Spectrum')
    ax6.bar(_[5:10], time2[5:10], color='blue', label='Yans')
    for i, v in enumerate(time2):
        tot_throug = round(experiment2[i][0] + experiment2[i][1], 2)
        plt.text(i, v + 0.05, tot_throug, color='black', ha='center')
    ax6.set_ylabel('Sim time [s]')
    ax6.set_xticklabels([])
    ax6.set_xticks([])
    ax6.grid(axis='y', linestyle='dashed', linewidth=0.5)
    ax6.set_title("Exp2: CPU time and total throughput", fontsize='small')
    ax6.legend(fontsize='x-small', loc='upper left', bbox_to_anchor=(0.8, 0.0))


    ax4.figure.savefig('scratch/SecondAssignment1.png')
    ax5.figure.savefig('scratch/SecondAssignment2.png')
    ax6.figure.savefig('scratch/SecondAssignment3.png')

    _ = list(range(0, 31))
    spectrum_norm_times = normalized_times[:5] + normalized_times[10:15] + normalized_times[20:25]
    yans_norm_times = normalized_times[5:10] + normalized_times[15:20] + normalized_times[25:30]
    fig, ax7 = plt.subplots(figsize=(6, 4))
    ax7.bar(_[:15], spectrum_norm_times, color='red', label='Spectrum')
    ax7.bar(_[15:30], yans_norm_times, color='blue', label='Yans')
    ax7.set_ylabel('"Normalized" times')
    ax7.set_xticklabels([])
    ax7.set_xticks([])
    ax7.grid(axis='y', linestyle='dashed', linewidth=0.5)
    ax7.set_title("CPU time / total throughput", fontsize='small')
    ax7.legend(fontsize='x-small', loc='upper left', bbox_to_anchor=(0.8, 0.0))

    ax7.figure.savefig('scratch/SecondAssignment4.png')



if __name__ == "__main__":
    main()
    elapsed_time = time.time()-start_time
    print(f"Elapsed time for ns-3 simulations + scripts c++ + script python: {elapsed_time:.2f} seconds")

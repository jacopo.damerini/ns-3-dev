#include "ns3/boolean.h"
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/core-module.h"
#include "ns3/double.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/log.h"
#include "ns3/mobility-helper.h"
#include "ns3/mobility-module.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/string.h"
#include "ns3/trace-helper.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/uinteger.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"

#include <chrono>
#include <iomanip>

// Network topology:
//
//         Wi-Fi 192.168.1.0                                     Wi-Fi 192.168.2.0
//
//   STA0                       AP0                             AP1                       STA1
//    * <- distance_S0_to_A0 ->  * <---- distance_A0_to_A1 ----> * <- distance_A1_to_S1 -> *
//    |                          |                               |                         |
//  StaNds[0]                   ApNds[0]                       ApNds[1]                StaNds[1]
//

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("SecondAssignment");

void
writeNodeInfo(uint32_t i,
              std::string alias,
              ns3::Vector position,
              bool enableNet1AppLayer,
              std::ofstream& outputFile)
{
    if (enableNet1AppLayer)
    {
        // Print all
        outputFile << "Node alias: " << alias << "     Position: " << position << std::endl;
    }
    else
    {
        // Print only nodes AP0 and ST0
        if ((alias == "AP0") || (alias == "ST0"))
        {
            outputFile << "Node alias: " << alias << "     Position: " << position << std::endl;
        }
    }
}

int
main(int argc, char* argv[])
{
    auto script_start = std::chrono::high_resolution_clock::now();
    int32_t simID = 0;
    double simulationTime = 10;    // seconds
    double distance_S0_to_A0 = 1;  // meters
    double distance_A0_to_A1 = 10; // meters
    double distance_A1_to_S1 = 1;  // meters
    std::string wifiType = "Spectrum";
    std::string errorModelType = "ns3::NistErrorRateModel";
    bool shortGuardInterval = false;
    const uint32_t payloadSize = 1024;
    bool enablePcap = false;
    int32_t channelNumber0 = 1; // number from 1 to 14
    int32_t channelNumber1 = 6; // number from 1 to 14
    bool enableNet1AppLayer = true;

    Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(payloadSize));

    CommandLine cmd(__FILE__);
    cmd.AddValue("simID", "ID of the simulation", simID);
    cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
    cmd.AddValue("distanceS0A0", "meters between S0 and A0", distance_S0_to_A0);
    cmd.AddValue("distanceA0A1", "meters between A0 and A1", distance_A0_to_A1);
    cmd.AddValue("distanceA1S1", "meters between A1 and S1", distance_A1_to_S1);
    cmd.AddValue("wifiType", "select Spectrum or Yans", wifiType);
    cmd.AddValue("errorModelType",
                 "select ns3::NistErrorRateModel or ns3::YansErrorRateModel",
                 errorModelType);
    cmd.AddValue("shortGuardInterval", "Enable/disable short guard interval", shortGuardInterval);
    cmd.AddValue("enablePcap", "enable pcap output", enablePcap);
    cmd.AddValue("chNumber0",
                 "Desired 2.4 GHz channel for STA0 e AP0 (select 1 - 14)",
                 channelNumber0);
    cmd.AddValue("chNumber1",
                 "Desired 2.4 GHz channel for STA1 e AP1 (select 1 - 14)",
                 channelNumber1);
    cmd.AddValue("enableNet1AppLayer",
                 "Select if you want to activate the app layer on network 1",
                 enableNet1AppLayer);
    cmd.Parse(argc, argv);

    // Check if chNumber are in range 1-14 otherwise abort
    if ((channelNumber0 > 15 || channelNumber0 < 1) || (channelNumber1 > 15 || channelNumber1 < 1))
    {
        NS_FATAL_ERROR("chNumber0 and chNumber1 must be between 1 and 14. You've typed: "
                       << channelNumber0 << " " << channelNumber1);
    }

    // Set central frequencies: the first channel is at 2412 MHz and every channel has 5 MHz of
    // difference, except channel 14 that is at 2484 MHz
    double centralChannelFrequency0 =
        (channelNumber0 != 14) ? (2407e6 + channelNumber0 * 5e6) : 2484e6;
    double centralChannelFrequency1 =
        (channelNumber1 != 14) ? (2407e6 + channelNumber1 * 5e6) : 2484e6;

    NodeContainer wifiStaNodes;
    wifiStaNodes.Create(2);
    NodeContainer wifiApNodes;
    wifiApNodes.Create(2);

    // Add aliases to node
    Ptr<Node> node_STA0 = wifiStaNodes.Get(0);
    std::string alias_STA0 = "ST0";
    Names::Add(alias_STA0, node_STA0);

    Ptr<Node> node_STA1 = wifiStaNodes.Get(1);
    std::string alias_STA1 = "ST1";
    Names::Add(alias_STA1, node_STA1);

    Ptr<Node> node_AP0 = wifiApNodes.Get(0);
    std::string alias_AP0 = "AP0";
    Names::Add(alias_AP0, node_AP0);

    Ptr<Node> node_AP1 = wifiApNodes.Get(1);
    std::string alias_AP1 = "AP1";
    Names::Add(alias_AP1, node_AP1);

    NodeContainer allNodes;
    allNodes.Add(wifiStaNodes);
    allNodes.Add(wifiApNodes);

    YansWifiPhyHelper yansPhyHlpr;
    SpectrumWifiPhyHelper spectrumPhyHlpr;

    WifiHelper wifiHlpr;
    wifiHlpr.SetStandard(WIFI_STANDARD_80211n);

    WifiMacHelper macHlpr;

    Ssid ssid0 = Ssid("n_wifi0_2_4_GHz");
    Ssid ssid1 = Ssid("n_wifi1_2_4_GHz");

    StringValue DataRate = StringValue("HtMcs4");

    wifiHlpr.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                                     "DataMode",
                                     DataRate,
                                     "ControlMode",
                                     DataRate);

    NetDeviceContainer staDevice0;
    NetDeviceContainer staDevice1;
    NetDeviceContainer apDevice0;
    NetDeviceContainer apDevice1;

    if (wifiType == "Yans")
    {
        // Phy setup
        yansPhyHlpr.SetErrorRateModel(errorModelType);
        yansPhyHlpr.Set("TxPowerStart", DoubleValue(1)); // dBm
        yansPhyHlpr.Set("TxPowerEnd", DoubleValue(1));

        YansWifiChannelHelper yansChannel;
        yansChannel.AddPropagationLoss(
            "ns3::FriisPropagationLossModel",
            "Frequency",
            DoubleValue(((centralChannelFrequency0 + centralChannelFrequency1) / 2)));
        yansChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
        yansPhyHlpr.SetChannel(yansChannel.Create());

        // 0
        macHlpr.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid0));
        yansPhyHlpr.Set(
            "ChannelSettings",
            StringValue("{" + std::to_string(channelNumber0) + ", 20, BAND_2_4GHZ, 0}"));
        staDevice0 = wifiHlpr.Install(yansPhyHlpr, macHlpr, wifiStaNodes.Get(0));
        macHlpr.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid0));
        apDevice0 = wifiHlpr.Install(yansPhyHlpr, macHlpr, wifiApNodes.Get(0));

        // 1
        macHlpr.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid1));
        yansPhyHlpr.Set(
            "ChannelSettings",
            StringValue("{" + std::to_string(channelNumber1) + ", 20, BAND_2_4GHZ, 0}"));
        staDevice1 = wifiHlpr.Install(yansPhyHlpr, macHlpr, wifiStaNodes.Get(1));
        macHlpr.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid1));
        apDevice1 = wifiHlpr.Install(yansPhyHlpr, macHlpr, wifiApNodes.Get(1));
    }
    else if (wifiType == "Spectrum")
    {
        // Phy setup
        spectrumPhyHlpr.SetErrorRateModel(errorModelType);
        spectrumPhyHlpr.Set("TxPowerStart", DoubleValue(1)); // dBm
        spectrumPhyHlpr.Set("TxPowerEnd", DoubleValue(1));

        Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
        lossModel->SetFrequency(((centralChannelFrequency0 + centralChannelFrequency1) / 2));
        Ptr<ConstantSpeedPropagationDelayModel> delayModel =
            CreateObject<ConstantSpeedPropagationDelayModel>();
        Ptr<MultiModelSpectrumChannel> spectrumChannel = CreateObject<MultiModelSpectrumChannel>();
        spectrumChannel->AddPropagationLossModel(lossModel);
        spectrumChannel->SetPropagationDelayModel(delayModel);
        spectrumPhyHlpr.SetChannel(spectrumChannel);

        // 0
        macHlpr.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid0));
        spectrumPhyHlpr.Set(
            "ChannelSettings",
            StringValue("{" + std::to_string(channelNumber0) + ", 20, BAND_2_4GHZ, 0}"));
        staDevice0 = wifiHlpr.Install(spectrumPhyHlpr, macHlpr, wifiStaNodes.Get(0));
        macHlpr.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid0));
        apDevice0 = wifiHlpr.Install(spectrumPhyHlpr, macHlpr, wifiApNodes.Get(0));

        // 1
        macHlpr.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid1));
        spectrumPhyHlpr.Set(
            "ChannelSettings",
            StringValue("{" + std::to_string(channelNumber1) + ", 20, BAND_2_4GHZ, 0}"));
        staDevice1 = wifiHlpr.Install(spectrumPhyHlpr, macHlpr, wifiStaNodes.Get(1));
        macHlpr.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid1));
        apDevice1 = wifiHlpr.Install(spectrumPhyHlpr, macHlpr, wifiApNodes.Get(1));
    }
    else
    {
        NS_FATAL_ERROR("Unsupported WiFi type " << wifiType);
    }

    // Set guard interval
    Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/"
                "ShortGuardIntervalSupported",
                BooleanValue(shortGuardInterval));

    // Mobility
    double ap1_pos = distance_S0_to_A0 + distance_A0_to_A1;
    double s1_pos = distance_S0_to_A0 + distance_A0_to_A1 + distance_A1_to_S1;

    // Posizione del nodo 0 e 1 WiFi STAnode
    MobilityHelper mobility_STAs;
    Ptr<ListPositionAllocator> positionAlloc_STAs = CreateObject<ListPositionAllocator>();

    positionAlloc_STAs->Add(Vector(0.0, 0.0, 0.0));
    positionAlloc_STAs->Add(Vector(s1_pos, 0.0, 0.0));
    mobility_STAs.SetPositionAllocator(positionAlloc_STAs);
    mobility_STAs.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility_STAs.Install(wifiStaNodes);

    // Posizione del nodo 0 e 1 WiFi APnode
    MobilityHelper mobility_APs;
    Ptr<ListPositionAllocator> positionAlloc_APs = CreateObject<ListPositionAllocator>();

    positionAlloc_APs->Add(Vector(distance_S0_to_A0, 0.0, 0.0));
    positionAlloc_APs->Add(Vector(ap1_pos, 0.0, 0.0));
    mobility_APs.SetPositionAllocator(positionAlloc_APs);
    mobility_APs.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility_APs.Install(wifiApNodes);

    /* Internet stack*/
    InternetStackHelper stack;
    stack.Install(wifiApNodes);
    stack.Install(wifiStaNodes);

    Ipv4AddressHelper address0;
    Ipv4AddressHelper address1;
    address0.SetBase("192.168.1.0", "255.255.255.0");
    address1.SetBase("192.168.2.0", "255.255.255.0");
    Ipv4InterfaceContainer staNodeInterface0;
    Ipv4InterfaceContainer staNodeInterface1;
    Ipv4InterfaceContainer apNodeInterface0;
    Ipv4InterfaceContainer apNodeInterface1;

    staNodeInterface0 = address0.Assign(staDevice0);
    staNodeInterface1 = address1.Assign(staDevice1);
    apNodeInterface0 = address0.Assign(apDevice0);
    apNodeInterface1 = address1.Assign(apDevice1);

    // Application level
    ApplicationContainer sink_STA0;
    ApplicationContainer sink_STA1;

    // TCP flow
    uint16_t port = 50000;

    // 0
    PacketSinkHelper packetSinkHelper0("ns3::TcpSocketFactory",
                                       InetSocketAddress(staNodeInterface0.GetAddress(0), port));
    sink_STA0 = packetSinkHelper0.Install(wifiStaNodes.Get(0));
    sink_STA0.Start(Seconds(0.0));
    sink_STA0.Stop(Seconds(simulationTime + 1));

    OnOffHelper onoff0("ns3::TcpSocketFactory",
                       InetSocketAddress(apNodeInterface0.GetAddress(0), port));
    onoff0.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
    onoff0.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
    onoff0.SetAttribute("PacketSize", UintegerValue(payloadSize));
    onoff0.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
    AddressValue remoteAddress0(InetSocketAddress(staNodeInterface0.GetAddress(0), port));
    onoff0.SetAttribute("Remote", remoteAddress0);
    ApplicationContainer onoff_AP0 = onoff0.Install(wifiApNodes.Get(0));
    onoff_AP0.Start(Seconds(1.0));
    onoff_AP0.Stop(Seconds(simulationTime + 1));

    // 1
    if (enableNet1AppLayer)
    {
        PacketSinkHelper packetSinkHelper1(
            "ns3::TcpSocketFactory",
            InetSocketAddress(staNodeInterface1.GetAddress(0), port));
        sink_STA1 = packetSinkHelper1.Install(wifiStaNodes.Get(1));
        sink_STA1.Start(Seconds(0.0));
        sink_STA1.Stop(Seconds(simulationTime + 1));

        OnOffHelper onoff1("ns3::TcpSocketFactory",
                           InetSocketAddress(apNodeInterface1.GetAddress(0), port));
        onoff1.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff1.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff1.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff1.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
        AddressValue remoteAddress1(InetSocketAddress(staNodeInterface1.GetAddress(0), port));
        onoff1.SetAttribute("Remote", remoteAddress1);
        ApplicationContainer onoff_AP1 = onoff1.Install(wifiApNodes.Get(1));
        onoff_AP1.Start(Seconds(1.0));
        onoff_AP1.Stop(Seconds(simulationTime + 1));
    }

    Simulator::Stop(Seconds(simulationTime + 1));

    if (enablePcap)
    {
        if (wifiType == "Yans")
        {
            std::string pcapFilePath = "scratch/yans_sim" + std::to_string(simID);
            yansPhyHlpr.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
            yansPhyHlpr.EnablePcap(pcapFilePath, apDevice0);
            yansPhyHlpr.EnablePcap(pcapFilePath, apDevice1);
        }
        else if (wifiType == "Spectrum")
        {
            std::string pcapFilePath = "scratch/spectrum_sim" + std::to_string(simID);
            spectrumPhyHlpr.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
            spectrumPhyHlpr.EnablePcap(pcapFilePath, apDevice0);
            spectrumPhyHlpr.EnablePcap(pcapFilePath, apDevice1);
        }
    }

    auto sim_start = std::chrono::high_resolution_clock::now();
    Simulator::Run();

    Simulator::Destroy();

    // Simulation execution time
    auto sim_end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> sim_duration = sim_end - sim_start;
    double sim_duration_seconds = sim_duration.count();

    // Open the file
    std::string outputFilePath = "scratch/output_sim" + std::to_string(simID) + ".txt";
    std::ofstream outputFile(outputFilePath);

    // Write configuration
    outputFile << "Type: " << wifiType << ";  DisS0A0: " << distance_S0_to_A0
               << ";  DisA0A1: " << distance_A0_to_A1 << ";  DisA1S1: " << distance_A1_to_S1
               << ";  enableNet1AppLayer: " << enableNet1AppLayer
               << ";  chNumber0: " << channelNumber0 << ";  chNumber1: " << channelNumber1
               << ";  cshortGuard: " << shortGuardInterval << std::endl;

    double throughput0 = 0;
    double throughput1 = 0;

    // 0
    uint64_t totalBytesRx0 = DynamicCast<PacketSink>(sink_STA0.Get(0))->GetTotalRx();
    throughput0 = totalBytesRx0 * 8 / (simulationTime * 1000000.0); // Mbit/s
    outputFile << "Throughput Network0: " << std::fixed << std::setprecision(2) << throughput0
               << " Mbit/s" << std::endl;

    // 1
    if (enableNet1AppLayer)
    {
        uint64_t totalBytesRx1 = DynamicCast<PacketSink>(sink_STA1.Get(0))->GetTotalRx();
        throughput1 = totalBytesRx1 * 8 / (simulationTime * 1000000.0); // Mbit/s
        outputFile << "Throughput Network1: " << std::fixed << std::setprecision(2) << throughput1
                   << " Mbit/s" << std::endl;
    }

    for (uint32_t i = 0; i < allNodes.GetN(); ++i)
    {
        // Get node positions
        Ptr<MobilityModel> mobilityModel = allNodes.Get(i)->GetObject<MobilityModel>();
        Vector position = mobilityModel->GetPosition();

        // Get node aliases
        Ptr<Node> node = allNodes.Get(i);
        std::string alias = Names::FindName(node);

        // Write node info
        writeNodeInfo(i, alias, position, enableNet1AppLayer, outputFile);
    }

    // Total script execution time
    auto script_end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> script_duration = script_end - script_start;
    double script_duration_seconds = script_duration.count();

    outputFile << "Sim seconds: " << std::fixed << std::setprecision(4) << sim_duration_seconds
               << "    Script seconds: " << std::fixed << std::setprecision(4)
               << script_duration_seconds << std::endl;

    // Remember to close the file
    outputFile.close();

    std::cout << "Simulation " << simID << " finished ----> Sim seconds: " << std::fixed
              << std::setprecision(4) << sim_duration_seconds
              << "    Script seconds: " << std::fixed << std::setprecision(4)
              << script_duration_seconds << std::endl;

    return 0;
}
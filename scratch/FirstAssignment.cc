#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/network-module.h"
#include "ns3/gnuplot-helper.h"

// Default Network Topology
// BS   SIN  BS   SIN
// n0   n1   n2   n3
// |    |    |    |
// ================
//   LAN 10.1.1.0
//

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("FirstAssignment");

int
main(int argc, char* argv[])
{   
    double simulationLenght = 8.0;
    int sendSize0 = 256;
    int sendSize2 = 1408;
    uint32_t seedValue = 0;
    std::string fileName="";
    double errorRate = 0.001;

    CommandLine cmd(__FILE__);
    cmd.AddValue("simulationLenght", "Simulation lenght in seconds", simulationLenght);
    cmd.AddValue("sendSize0", "Size of packets send by bulk sender 0", sendSize0);
    cmd.AddValue("sendSize2", "Size of packets send by bulk sender 2", sendSize2);
    cmd.AddValue("seedValue", "Value of the seed", seedValue);
    cmd.AddValue("fileName", "Name of the saved file", fileName);
    cmd.AddValue("errorRate", "Error rate", errorRate);
    cmd.Parse(argc, argv);
    std::cout <<"Error rate: " << errorRate << "     Seed value: " << seedValue << "     File xml to save: " << fileName << std::endl;

    ns3::SeedManager::SetSeed(seedValue);

    Time::SetResolution(Time::NS);

    Ptr<UniformRandomVariable> uv = CreateObject<UniformRandomVariable>();
    uv->SetStream(50);
    RateErrorModel error_model;
    error_model.SetRandomVariable(uv);
    error_model.SetUnit(RateErrorModel::ERROR_UNIT_PACKET);
    error_model.SetRate(errorRate);

    NodeContainer csmaNodes;
    csmaNodes.Create(4);

    CsmaHelper csma;
    csma.SetChannelAttribute("DataRate", StringValue("100Mbps"));
    csma.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6560)));
    csma.SetDeviceAttribute("ReceiveErrorModel", PointerValue(&error_model));

    NetDeviceContainer csmaDevices;
    csmaDevices = csma.Install(csmaNodes);

    InternetStackHelper stack;
    stack.Install(csmaNodes);

    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");

    Ipv4InterfaceContainer csmaInterfaces;
    csmaInterfaces = address.Assign(csmaDevices);

    uint16_t sinkPort = 8080;
    BulkSendHelper bulkSender_0to1("ns3::TcpSocketFactory", InetSocketAddress(csmaInterfaces.GetAddress(1), sinkPort));
    BulkSendHelper bulkSender_2to3("ns3::TcpSocketFactory", InetSocketAddress(csmaInterfaces.GetAddress(3), sinkPort));
    bulkSender_0to1.SetAttribute("SendSize", UintegerValue(1024));
    bulkSender_0to1.SetAttribute("MaxBytes", UintegerValue(0));
    bulkSender_2to3.SetAttribute("SendSize", UintegerValue(1400));
    bulkSender_2to3.SetAttribute("MaxBytes", UintegerValue(0));
    ApplicationContainer bulkSenderApp0 = bulkSender_0to1.Install(csmaNodes.Get(0));
    ApplicationContainer bulkSenderApp2 = bulkSender_2to3.Install(csmaNodes.Get(2));
    bulkSenderApp0.Start(Seconds(0.0));
    bulkSenderApp2.Start(Seconds(0.0));
    bulkSenderApp0.Stop(Seconds(simulationLenght));
    bulkSenderApp2.Stop(Seconds(simulationLenght));


    PacketSinkHelper packetSink1("ns3::TcpSocketFactory", InetSocketAddress(csmaInterfaces.GetAddress(1), sinkPort));
    PacketSinkHelper packetSink3("ns3::TcpSocketFactory", InetSocketAddress(csmaInterfaces.GetAddress(3), sinkPort));
    ApplicationContainer sinkApp1 = packetSink1.Install(csmaNodes.Get(1));
    ApplicationContainer sinkApp3 = packetSink3.Install(csmaNodes.Get(3));
    sinkApp1.Start(Seconds(0.0));
    sinkApp3.Start(Seconds(0.0));
    sinkApp1.Stop(Seconds(simulationLenght));
    sinkApp3.Stop(Seconds(simulationLenght));

    Ipv4GlobalRoutingHelper::PopulateRoutingTables();

    FlowMonitorHelper flowMonitor;
    Ptr<FlowMonitor> monitor = flowMonitor.Install(csmaNodes);

    Simulator::Stop(Seconds(simulationLenght));
    Simulator::Run();
    Simulator::Destroy();
    
    std::cout << "Simulation finished, exporting data " << std::endl;
    monitor->SerializeToXmlFile(fileName, true, true);
    std::cout << "Data exported" << std::endl;

    return 0;
}